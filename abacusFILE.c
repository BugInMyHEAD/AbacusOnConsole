#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

struct setting;
int quit();
int playAbacus();
int printAbacus(const char* digit);
int initAbacus(char* digit, int valInit);
int question();
int flick(char* digit);
int confAbacus(const struct setting* setting_list);
int getint();
int arrange_setting(struct setting* setting_list);
int recommend_setting(const char* filename, const struct setting* setting_list);

int qstbelow, cstrandmax, abacusmax, DIGITS = 5, QSTDIGITS = 4;
int DVORAK = 0;

int main()
{
    int i;
    int (*tool[])() = {
        quit, playAbacus
    };
    
    i = 1;
    if(tool[i]()) return i;
    
    return 0;
}

int quit()
{
    return -1;
}

struct setting {
    char *called;
    int *matched;
};

int arrange_setting(struct setting* setting_list)
{
    int count = 0;
    struct setting setting_tmp, *above = setting_list, *under = NULL;
    
    while(above->called)
    {
        for(under = above + 1; under->called; ++under)
        {
            if(strstr(under->called, above->called))
            {
                setting_tmp = *above;
                *above = *under;
                *under = setting_tmp;
                ++count;
            }
        }
        ++above;
    }
    
    return count;
}

int recommend_setting(const char* filename, const struct setting* setting_list)
{
    FILE *recommend = NULL;
    int i;
    
    if(NULL == (recommend = fopen(filename, "wt"))) return -1;
    
    fputs("#CAUTION : virtual variables following '&' in here may not be the same as in \n#the source code, which are just literally copied\n", recommend);
    for(i = 0; setting_list[i].called; ++i)
    {
        fputs("\n", recommend);
        fprintf(recommend, "\"%s\", &%s,", setting_list[i].called, setting_list[i].called);
    }
    fputs("\nNULL, NULL", recommend);
    
    fclose(recommend);
    
    return i;
}

int playAbacus()
{
    char *digit = NULL;//[DIGITS];
    int i, j, answer;
    static int executions = 0;
    //int k;
    
    static struct setting setting_list[] = {
        {"DVORAK", &DVORAK},
        {"DIGITS", &DIGITS},
        {"QSTDIGITS", &QSTDIGITS},
        {NULL, NULL}
    };
    
    //if(0 == executions) if(arrange_setting(setting_list)) recommend_setting("_Optimized setting_list for Abacus.txt",setting_list);
    ++executions;
    
    confAbacus(setting_list);
    if(DIGITS > 10)
    {
        printf("DIGITS > 10");
        
        return 0;
    }
    if((qstbelow = (int)pow(10, QSTDIGITS)) - 1 > RAND_MAX)
    {
        printf("qstbelow - 1 > RAND_MAX");
        
        return 0;
    }
    cstrandmax = RAND_MAX - RAND_MAX % qstbelow - 1;
    abacusmax = (int)pow(10, DIGITS) - 1;
    srand((unsigned int)time(NULL));
    
    if(digit = (char*)malloc(DIGITS * sizeof(*digit)));
    
    do
    {
        i = question();
        j = question();
        answer = i + j;
        do
        {
            if(initAbacus(digit, 0))
            {
                printf("initAbacus != 0");
                
                return 0;
            }
            do
            {
                system("cls");
                printf("Calculate %d + %d\n", i, j);
                //for(k = 0; k < DIGITS; ++k) printf("%#x ", digit[k]);
                //putchar('\n');
                printAbacus(digit);
                printf("\t\t(NO CAPITALS)\nPlus/minus 5:\tqwertyuiop\nPlus 1:\t\tasdfghjkl;\nMinus 1:\tzxcvbnm,./\nAnswer:\t\tEnter\n");
            } while(/*k = */flick(digit));
            printf("The answer is :\t");
        } while(getint() != answer);
    } while(1);
    
    free(digit);
    
    return 0;
}

int printAbacus(const char* digit)
{
    int i;
    char comb = 0x40;
    
    printf("��");
    for(i = 0; i < DIGITS; ++i) printf("��");
    puts("��");
    do
    {
        printf("��");
        for(i = 0; i < DIGITS; ++i)
        {
            if(digit[i] & comb) printf("��");
            else printf("��");
        }
        puts("��");
    } while(0x60 & (comb >>= 1));
    printf("��");
    for(i = 0; i < DIGITS; ++i) printf("��");
    puts("��");
    do
    {
        printf("��");
        for(i = 0; i < DIGITS; ++i)
        {
            if(digit[i] & comb) printf("��");
            else printf("��");
        }
        puts("��");
    } while(comb >>= 1);
    printf("��");
    for(i = 0; i < DIGITS; ++i) printf("��");
    puts("��");
    
    return 0;
}

int initAbacus(char* digit, int valInit)
{
    int i, mod;
    
    if(abacusmax < valInit) return abacusmax;
    if(valInit < 0) return -1;
    
    for(i = DIGITS - 1; 0 <= i; --i)
    {
        mod = valInit % 10;
        digit[i] = 0x7F & 0xBF >> (mod < 5);
        mod %= 5;
        digit[i] &= 0xEF >> mod;
        valInit /= 10;
    }
    
    return 0;
}

int question()
{
    int i;
    
    do
    {
        i = rand();
    }
    while(i > cstrandmax);
    
    return i % qstbelow;
}

int getint()
{
    int i;
    
    do
    {
        fflush(stdin);
    }
    while(!scanf("%d", &i));
    
    return i;
}

int flick(char* digit)
{
    int i, j;
    char c;
    
    switch(getch())
    {
#define ABACUS_KEY(a, b, c) case (a): i = (b); j = (c); break
    case '\r':
        return 0;
        break;
        ABACUS_KEY('q', 0, 0);
        ABACUS_KEY('w', 1, 0);
        ABACUS_KEY('e', 2, 0);
        ABACUS_KEY('r', 3, 0);
        ABACUS_KEY('t', 4, 0);
        ABACUS_KEY('y', 5, 0);
        ABACUS_KEY('u', 6, 0);
        ABACUS_KEY('i', 7, 0);
        ABACUS_KEY('o', 8, 0);
        ABACUS_KEY('p', 9, 0);
        ABACUS_KEY('a', 0, 1);
        ABACUS_KEY('s', 1, 1);
        ABACUS_KEY('d', 2, 1);
        ABACUS_KEY('f', 3, 1);
        ABACUS_KEY('g', 4, 1);
        ABACUS_KEY('h', 5, 1);
        ABACUS_KEY('j', 6, 1);
        ABACUS_KEY('k', 7, 1);
        ABACUS_KEY('l', 8, 1);
        ABACUS_KEY(';', 9, 1);
        ABACUS_KEY('z', 0, -1);
        ABACUS_KEY('x', 1, -1);
        ABACUS_KEY('c', 2, -1);
        ABACUS_KEY('v', 3, -1);
        ABACUS_KEY('b', 4, -1);
        ABACUS_KEY('n', 5, -1);
        ABACUS_KEY('m', 6, -1);
        ABACUS_KEY(',', 7, -1);
        ABACUS_KEY('.', 8, -1);
        ABACUS_KEY('/', 9, -1);
    default:
        return -1;
        break;
#undef ABACUS_KEY
    }
    
    if(DIGITS <= i) return -2;
    switch(j)
    {
    case -1:
        if(digit[i] & 0x10)
        {
            //digit[i] ^= (digit[i] << 1 | 0xE1) ^ (digit[i] | 0xE0);
            c = ~digit[i] & 0x1F;
            digit[i] ^= c | c << 1;
        }
        else return 2;
        break;
    case 0:
        digit[i] ^= 0x60;
        break;
    case 1:
        if(digit[i] & 0x01)
        {
            //digit[i] ^= (digit[i] >> 1 | 0xF0) ^ (digit[i] | 0xE0);
            c = ~digit[i] & 0x1F;
            digit[i] ^= c | c >> 1;
        }
        else return 3;
        break;
    }
    return 1;
}

int confAbacus(const struct setting* setting_list)
{
    //char strbuf[BUFSIZ];
    char wordbuf[BUFSIZ];
    FILE *conf = NULL;
    char *pchar = NULL;
    int *pint = NULL;
    const struct setting* psetting = NULL;
    
    if(conf = fopen("conf_Abacus.txt", "r+t"))
    {
        while(EOF != fscanf(conf, "%s", wordbuf))
        {
            if(pchar = strpbrk(wordbuf, "#")) *pchar = '\0';
            //strupr(wordbuf);
            
            if(pint && sscanf(wordbuf, "%d", pint)) pint = NULL;
            else for(psetting = setting_list; psetting->called; ++psetting)
            {
                if(0 == strcasecmp(wordbuf, psetting->called))
                {
                    pint = psetting->matched;
                    break;
                }
            }
            
            if(pchar) fgets(wordbuf, BUFSIZ, conf);
        }
    } else
    {
        conf = fopen("conf_Abacus.txt", "wt");
        fprintf(conf, "DVORAK 0 #not realized yet\nDIGITS %d\nQSTDIGITS %d", DIGITS, QSTDIGITS);
    }
    fclose(conf);
    
    return 0;
}
